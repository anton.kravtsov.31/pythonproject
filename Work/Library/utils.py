from tkinter import *

def get_data(entries: list[Entry]) -> list:
    data = []
    for entry in entries:
        data.append(entry.get())
    return data

def all_children (window) :
    _list = window.winfo_children()

    for item in _list :
        if item.winfo_children() :
            _list.extend(item.winfo_children())

    return _list