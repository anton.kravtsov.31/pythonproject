import sqlite3
import pandas as pd


class DBConnector:
    def __init__(self):
        self.__connect = sqlite3.connect('../Data/data.db')
        self.__cursor = self.__connect.cursor()

    def terminate(self):
        self.__connect.close()

    def get_games_data_frame(self):
        df = pd.read_sql("SELECT * FROM games", self.__connect)
        return df

    def get_devs_data_frame(self):
        df = pd.read_sql("SELECT * FROM developers", self.__connect)
        return df

    def save_games_data_frame(self, df: pd.DataFrame):
        df.to_sql('games', self.__connect)

    def save_devs_data_frame(self, df: pd.DataFrame):
        df.to_sql('developers', self.__connect)
