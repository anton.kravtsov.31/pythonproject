import tkinter.filedialog as fd
from Work.Scripts.dbconnect import DBConnector
import pandas as pd
from tkinter import ttk
import tkinter.messagebox as mb
from Work.Library.utils import *
from pathlib import *
import matplotlib.pyplot as plt

class DataMiner:
    def __init__(self):
        self.games_df = pd.DataFrame()
        self.devs_df = pd.DataFrame()
        self.dbconnector = DBConnector()
        self.update_dfs_from_db()

    def update_dfs_from_db(self):
        self.games_df = self.dbconnector.get_games_data_frame()
        self.devs_df = self.dbconnector.get_devs_data_frame()

    def save_dfs_in_db(self):
        self.dbconnector.save_devs_data_frame(self.devs_df)
        self.dbconnector.save_games_data_frame(self.games_df)

    def dev_exists(self, name: str) -> bool:
        if name in self.devs_df['Name'].values:
            return True
        return False

    def add_game(self, data: list, _id=-1):
        try:
            data[4] = int(data[4])
            data[5] = int(data[5])
            data[2] = int(data[2])
        except Exception as e:
            print(e.args[0])
            mb.showerror("Ошибка добавления", 'Проверьте правильность заполняемых данных')

        else:
            if self.dev_exists(data[1]):
                new_id = self.games_df['GameID'].max() + 1
                if _id != -1:
                    new_id = _id
                new_row = {
                    'GameID': new_id,
                    'GameName': data[0],
                    'Developer': data[1],
                    'Year': data[2],
                    'Genre': data[3],
                    'PeakPlayers': data[4],
                    'Rating': data[5]
                }

                self.games_df = self.games_df.append(new_row, ignore_index=True)
            else:
                mb.showerror("Ошибка добавления",
                             "Такого разработчика не существует. Прежде, чем добавлять игру, добавьте разработчика")

    def delete_game(self, id: int):
        self.games_df = self.games_df.loc[self.games_df['GameID'] != id]

    def add_dev(self, data: list, _id=-1):
        try:
            data[1] = int(data[1])
        except Exception as e:
            print(e.args[0])
            mb.showerror("Ошибка добавления", 'Проверьте правильность заполняемых данных')

        else:
            new_id = self.devs_df['DeveloperID'].max() + 1
            if _id != -1:
                new_id = _id
            new_row = {
                'DeveloperID': new_id,
                'Name': data[0],
                'NumberOfEmployees': data[1],
                'City': data[2],
            }

            self.devs_df = self.devs_df.append(new_row, ignore_index=True)

    def delete_dev(self, id: int, delete_games=True):
        if delete_games:
            self.delete_games_from_dev(self.get_dev_name(id))
        self.devs_df = self.devs_df.loc[self.devs_df['DeveloperID'] != id]

    def delete_games_from_dev(self, name: str):
        self.games_df = self.games_df.loc[self.games_df['Developer'] != name]

    def get_dev_name(self, _id: int):
        name = self.devs_df.loc[self.devs_df['DeveloperID'] == _id].values[0][1]
        print(name)
        return name


class AddGameWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super().__init__(parent)
        self.__init_UI()
        self.data: tuple
        self.data_miner = data_miner

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        labels = ['GameName', 'Developer', 'Year', 'Genre', 'PeakPlayers', 'Rating']
        entries = []
        for idx, text in enumerate(labels):
            # Создает ярлык с текстом из списка ярлыков.
            label = Label(master=frm_form, text=text)
            # Создает текстовое поле которая соответствует ярлыку.
            entry = Entry(master=frm_form, width=50)
            entries.append(entry)
            # Использует менеджер геометрии grid для размещения ярлыков и
            # текстовых полей в строку, чей индекс равен idx.
            label.grid(row=idx, column=0, sticky="e")
            entry.grid(row=idx, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Добавить", command=lambda: self.submit(entries))
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self, entries: list[Entry]):
        data = get_data(entries)
        self.destroy()
        self.data_miner.add_game(data)


class DeleteGameWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super().__init__(parent)
        self.__init_UI()
        self.data_miner = data_miner

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        label_text = 'GameID'
        label = Label(master=frm_form, text=label_text)
        self.entry = Entry(master=frm_form, width=50)
        label.grid(row=0, column=0, sticky="e")
        self.entry.grid(row=0, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Удалить", command=self.submit)
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self):
        id = int(self.entry.get())
        self.destroy()
        self.data_miner.delete_game(id)


class EditGameWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super().__init__(parent)
        self.__init_UI()
        self.data_miner = data_miner
        self.id: int

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        label_text = 'GameID'
        label = Label(master=frm_form, text=label_text)
        self.entry = Entry(master=frm_form, width=50)
        label.grid(row=0, column=0, sticky="e")
        self.entry.grid(row=0, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Изменить", command=self.load_editor)
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def load_editor(self):
        self.id = int(self.entry.get())
        if self.id not in list(self.data_miner.games_df['GameID'].values):
            self.destroy()
            mb.showerror('Ошибка изменения', "Такой игры не существует")
            return
        widget_list = all_children(self)
        for item in widget_list:
            item.pack_forget()

        game_data = self.data_miner.games_df.loc[self.data_miner.games_df['GameID'] == self.id]
        game_data = list(game_data.iloc[0])

        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        labels = ['GameName', 'Developer', 'Year', 'Genre', 'PeakPlayers', 'Rating']
        entries = []
        for idx, text in enumerate(labels):
            # Создает ярлык с текстом из списка ярлыков.
            label = Label(master=frm_form, text=text)
            # Создает текстовое поле которая соответствует ярлыку.
            entry = Entry(master=frm_form, width=50)
            entry.insert(0, game_data[idx + 1])
            entries.append(entry)
            # Использует менеджер геометрии grid для размещения ярлыков и
            # текстовых полей в строку, чей индекс равен idx.
            label.grid(row=idx, column=0, sticky="e")
            entry.grid(row=idx, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Изменить", command=lambda: self.submit(entries))
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self, entries):
        data = get_data(entries)
        self.destroy()
        if not self.data_miner.dev_exists(data[1]):
            mb.showerror("Ошибка изменения",
                         "Такого разработчика не существует")
            return
        self.data_miner.delete_game(self.id)
        self.data_miner.add_game(data, _id=self.id)


class AddDevWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super(AddDevWindow, self).__init__(parent)
        self.__init_UI()
        self.data: tuple
        self.data_miner = data_miner

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        labels = ['Name', 'NumberOfEmployees', 'City']
        entries = []
        for idx, text in enumerate(labels):
            # Создает ярлык с текстом из списка ярлыков.
            label = Label(master=frm_form, text=text)
            # Создает текстовое поле которая соответствует ярлыку.
            entry = Entry(master=frm_form, width=50)
            entries.append(entry)
            # Использует менеджер геометрии grid для размещения ярлыков и
            # текстовых полей в строку, чей индекс равен idx.
            label.grid(row=idx, column=0, sticky="e")
            entry.grid(row=idx, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Добавить", command=lambda: self.submit(entries))
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self, entries: list[Entry]):
        data = get_data(entries)
        self.destroy()
        self.data_miner.add_dev(data)


class DeleteDevWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super().__init__(parent)
        self.__init_UI()
        self.data_miner = data_miner

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        label_text = 'DeveloperID'
        label = Label(master=frm_form, text=label_text)
        self.entry = Entry(master=frm_form, width=50)
        label.grid(row=0, column=0, sticky="e")
        self.entry.grid(row=0, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Удалить", command=self.submit)
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self):
        id = int(self.entry.get())
        self.destroy()
        self.data_miner.delete_dev(id)


class EditDevWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super().__init__(parent)
        self.__init_UI()
        self.data_miner = data_miner
        self.id: int
        self.old_name: str

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        label_text = 'DeveloperID'
        label = Label(master=frm_form, text=label_text)
        self.entry = Entry(master=frm_form, width=50)
        label.grid(row=0, column=0, sticky="e")
        self.entry.grid(row=0, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Изменить", command=self.load_editor)
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def load_editor(self):
        self.id = int(self.entry.get())
        if self.id not in list(self.data_miner.devs_df['DeveloperID'].values):
            self.destroy()
            mb.showerror('Ошибка изменения', "Такого разработчика не существует")
            return
        widget_list = all_children(self)
        for item in widget_list:
            item.pack_forget()

        dev_data = self.data_miner.devs_df.loc[self.data_miner.devs_df['DeveloperID'] == self.id]
        dev_data = list(dev_data.iloc[0])
        self.old_name = dev_data[1]

        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()
        labels = ['Name', 'NumberOfEmployees', 'City']
        entries = []
        for idx, text in enumerate(labels):
            # Создает ярлык с текстом из списка ярлыков.
            label = Label(master=frm_form, text=text)
            # Создает текстовое поле которая соответствует ярлыку.
            entry = Entry(master=frm_form, width=50)
            entry.insert(0, dev_data[idx + 1])
            entries.append(entry)
            # Использует менеджер геометрии grid для размещения ярлыков и
            # текстовых полей в строку, чей индекс равен idx.
            label.grid(row=idx, column=0, sticky="e")
            entry.grid(row=idx, column=1)
        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Изменить", command=lambda: self.submit(entries))
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self, entries):
        data = get_data(entries)
        self.destroy()
        self.data_miner.delete_dev(self.id, delete_games=False)
        self.data_miner.add_dev(data, _id=self.id)
        print(self.old_name)
        print(data[0])
        self.data_miner.games_df = self.data_miner.games_df.replace({'Developer': {self.old_name: data[0]}})


class FilterGameWindow(Toplevel):
    def __init__(self, parent, data_miner: DataMiner):
        super(FilterGameWindow, self).__init__(parent)
        self.data_miner = data_miner
        self.__init_UI()

    def __init_UI(self):
        frm_form = Frame(self, relief=SUNKEN, borderwidth=3)
        frm_form.pack()

        lbl_year_min = Label(frm_form, text='Минимальный год')
        lbl_year_min.grid(row=0, column=0, sticky='e')
        self.scl_year_min = Scale(frm_form, from_=self.data_miner.games_df['Year'].min(),
                                  to=self.data_miner.games_df['Year'].max(), orient="horizontal")
        self.scl_year_min.grid(row=0, column=1)

        lbl_year_max = Label(frm_form, text='Максимальный год')
        lbl_year_max.grid(row=1, column=0, sticky='e')
        self.scl_year_max = Scale(frm_form, from_=self.data_miner.games_df['Year'].min(),
                                  to=self.data_miner.games_df['Year'].max(), orient="horizontal")
        self.scl_year_max.grid(row=1, column=1)

        lbl_peak_min = Label(frm_form, text='Минимальный пик')
        lbl_peak_min.grid(row=2, column=0, sticky='e')
        self.scl_peak_min = Scale(frm_form, from_=self.data_miner.games_df['PeakPlayers'].min(),
                                  to=self.data_miner.games_df['PeakPlayers'].max(), orient="horizontal")
        self.scl_peak_min.grid(row=2, column=1)

        lbl_peak_max = Label(frm_form, text='Максимальный пик')
        lbl_peak_max.grid(row=3, column=0, sticky='e')
        self.scl_peak_max = Scale(frm_form, from_=self.data_miner.games_df['PeakPlayers'].min(),
                                  to=self.data_miner.games_df['PeakPlayers'].max(), orient="horizontal")
        self.scl_peak_max.grid(row=3, column=1)

        lbl_rating_min = Label(frm_form, text='Минимальный рейтинг')
        lbl_rating_min.grid(row=4, column=0, sticky='e')
        self.scl_rating_min = Scale(frm_form, from_=self.data_miner.games_df['Rating'].min(),
                                    to=self.data_miner.games_df['Rating'].max(), orient="horizontal")
        self.scl_rating_min.grid(row=4, column=1)

        lbl_rating_max = Label(frm_form, text='Максимальный рейтинг')
        lbl_rating_max.grid(row=5, column=0, sticky='e')
        self.scl_rating_max = Scale(frm_form, from_=self.data_miner.games_df['Rating'].min(),
                                    to=self.data_miner.games_df['Rating'].max(), orient="horizontal")
        self.scl_rating_max.grid(row=5, column=1)

        frm_buttons = Frame(self)
        frm_buttons.pack(fill=X, ipadx=5, ipady=5)
        btn_submit = Button(master=frm_buttons, text="Фильтровать", command=self.submit)
        btn_submit.pack(side=RIGHT, padx=10, ipadx=10)

    def submit(self):
        year_min = self.scl_year_min.get()
        year_max = self.scl_year_max.get()
        peak_min = self.scl_peak_min.get()
        peak_max = self.scl_peak_max.get()
        rating_min = self.scl_rating_min.get()
        rating_max = self.scl_rating_max.get()

        self.data_miner.games_df = self.data_miner.games_df.loc[
            (self.data_miner.games_df['Year'] >= year_min) &
            (self.data_miner.games_df['Year'] <= year_max) &
            (self.data_miner.games_df['PeakPlayers'] >= peak_min) &
            (self.data_miner.games_df['PeakPlayers'] <= peak_max) &
            (self.data_miner.games_df['Rating'] >= rating_min) &
            (self.data_miner.games_df['Rating'] <= rating_max)
            ]
        self.destroy()


class MainFrame(Frame):

    def __init__(self, data_miner: DataMiner):
        super().__init__()
        self.data_miner = data_miner
        self.initUI()

    def initUI(self):
        self.master.title("Проект Python")
        self.pack(fill=BOTH, expand=True)
        menu_bar = Menu(self.master)
        self.master.config(menu=menu_bar)
        file_menu = Menu(menu_bar, tearoff=0)
        file_menu.add_command(label='Сохранить изменения в базе данных')
        file_menu.add_command(label='Откатить текущие изменения', command=self.revert_changes)

        show_menu = Menu(file_menu, tearoff=0)
        show_menu.add_command(label='Игры', command=self.show_games_table)
        show_menu.add_command(label='Разработчики', command=self.show_devs_table)
        file_menu.add_cascade(label='Показать таблицу', menu=show_menu)

        file_menu.add_separator()
        file_menu.add_command(label='Выход', command=self.quit)

        menu_bar.add_cascade(label='Файл', menu=file_menu)

        edit_menu = Menu(menu_bar, tearoff=0)

        edit_game_menu = Menu(edit_menu, tearoff=0)
        edit_game_menu.add_command(label='Добавить игру', command=self.handle_game_add)
        edit_game_menu.add_command(label='Удалить игру', command=self.handle_delete_game)
        edit_game_menu.add_command(label='Изменить игру', command=self.handle_edit_game)
        edit_menu.add_cascade(label='Игра', menu=edit_game_menu)

        edit_dev_menu = Menu(edit_menu, tearoff=0)
        edit_dev_menu.add_command(label='Добавить разработчика', command=self.handle_add_dev)
        edit_dev_menu.add_command(label='Удалить разработчика', command=self.handle_delete_dev)
        edit_dev_menu.add_command(label='Изменить разработчика', command=self.handle_edit_dev)

        edit_menu.add_cascade(label='Разработчик', menu=edit_dev_menu)

        menu_bar.add_cascade(label='Редактирование', menu=edit_menu)

        report_menu = Menu(edit_menu, tearoff=0)
        report_menu.add_command(label='Фильтровать игры', command=self.handle_filter_games)
        report_menu.add_command(label='Сгенерировать текстовый отчет', command=self.handle_gen_text_report)

        graph_menu = Menu(report_menu, tearoff=0)
        graph_menu.add_command(label='Средний рейтинг игр за год', command=self.handle_show_graph1)
        graph_menu.add_command(label='Рейтинг игр от количества сотрудников', command=self.handle_show_graph2)
        graph_menu.add_command(label='Рейтинг игр от пика игроков', command=self.handle_show_graph3)
        report_menu.add_cascade(label='Графики', menu=graph_menu)

        menu_bar.add_cascade(label='Отчеты', menu=report_menu)

        self.games_table = ttk.Treeview(self, show='headings', selectmode='browse')
        self.update_games_data_from_df()
        self.games_scrollbar = Scrollbar(self, command=self.games_table.yview)
        self.games_table.configure(yscrollcommand=self.games_scrollbar.set)
        self.games_scrollbar.pack(side=RIGHT, fill=Y)
        self.games_table.pack(expand=YES, fill=BOTH)

        self.devs_table = ttk.Treeview(self, show='headings', selectmode='browse')
        self.update_devs_data_from_df()
        self.devs_scrollbar = Scrollbar(self, command=self.devs_table.yview)
        self.devs_table.configure(yscrollcommand=self.devs_scrollbar.set)

    def show_games_table(self):
        self.devs_table.pack_forget()
        self.devs_scrollbar.pack_forget()
        self.games_scrollbar.pack(side=RIGHT, fill=Y)
        self.games_table.pack(expand=YES, fill=BOTH)

    def show_devs_table(self):
        self.games_table.pack_forget()
        self.games_scrollbar.pack_forget()
        self.devs_scrollbar.pack(side=RIGHT, fill=Y)
        self.devs_table.pack(expand=YES, fill=BOTH)

    def update_devs_data_from_df(self):
        headings = ['DeveloperID', 'Name', 'NumberOfEmployees', 'City']
        self.devs_table['columns'] = headings
        data = self.data_miner.devs_df

        for heading in headings:
            key = str
            if heading == 'DeveloperID' or heading == 'NumberOfEmployees':
                key = int
            self.devs_table.heading(heading, text=heading, anchor=CENTER,
                                    command=lambda heading_=heading, key_=key:
                                    self.treeview_sort_column(self.devs_table, heading_, False, key=key_))
            self.devs_table.column(heading, anchor=CENTER)
            self.devs_table.heading(heading, text=heading, anchor=CENTER)
            self.devs_table.column(heading, anchor=CENTER)

        self.devs_table.delete(*self.devs_table.get_children())
        for index, row in data.iterrows():
            self.devs_table.insert('', END, values=tuple(row))

    def update_games_data_from_df(self):
        headings = ['GameID', 'GameName', 'Developer', 'Year', 'Genre', 'PeakPlayers', 'Rating']
        self.games_table['columns'] = headings
        data = self.data_miner.games_df
        for heading in headings:
            key = str
            if heading == 'PeakPlayers' or heading == 'GameID':
                key = int
            self.games_table.heading(heading, text=heading, anchor=CENTER,
                                     command=lambda heading_=heading, key_=key:
                                     self.treeview_sort_column(self.games_table, heading_, False, key=key_))
            self.games_table.column(heading, anchor=CENTER)

        self.games_table.delete(*self.games_table.get_children())
        for index, row in data.iterrows():
            self.games_table.insert('', END, values=tuple(row))

    def treeview_sort_column(self, tv, col, reverse, key=str):
        lst = [(tv.set(k, col), k) for k in tv.get_children()]
        lst.sort(reverse=reverse, key=lambda t: key(t[0]))

        for index, (val, k) in enumerate(lst):
            tv.move(k, '', index)

        tv.heading(col, command=lambda: self.treeview_sort_column(tv, col, not reverse, key=key))

    def revert_changes(self):
        self.data_miner.update_dfs_from_db()
        self.update_games_data_from_df()
        self.update_devs_data_from_df()

    def handle_game_add(self):
        window = AddGameWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_games_data_from_df()

    def handle_delete_game(self):
        window = DeleteGameWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_games_data_from_df()

    def handle_edit_game(self):
        window = EditGameWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_games_data_from_df()

    def handle_add_dev(self):
        window = AddDevWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_devs_data_from_df()

    def handle_delete_dev(self):
        window = DeleteDevWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_devs_data_from_df()
        self.update_games_data_from_df()

    def handle_edit_dev(self):
        window = EditDevWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_devs_data_from_df()
        self.update_games_data_from_df()

    def handle_filter_games(self):
        window = FilterGameWindow(self, self.data_miner)
        window.grab_set()
        window.wait_window()
        self.update_games_data_from_df()

    def handle_gen_text_report(self):
        start_path = Path.cwd()
        filetypes = (("Текстовый файл", "*.txt"),)
        out_path = str(fd.asksaveasfilename(title='Сохранить в:', initialdir=start_path,
                                            filetypes=filetypes))
        if len(out_path) == 0:
            return

        if '.txt' not in out_path:
            out_path += '.txt'

        text = ""
        text += "Количество игр: " + str(self.data_miner.games_df['GameID'].count()) + "\n"
        text += "Количество разработчиков: " + str(self.data_miner.devs_df['DeveloperID'].count()) + "\n"
        text += "Максимальный рейтинг: " + str(self.data_miner.games_df['Rating'].max()) + "\n"
        text += "Средний рейтинг: " \
                + str(self.data_miner.games_df['Rating'].sum() / self.data_miner.games_df['Rating'].count()) + "\n"

        text += "Максимальный пик игроков: " + str(self.data_miner.games_df['PeakPlayers'].max()) + "\n"
        text += "Средний пик игроков: " \
                + str(self.data_miner.games_df['PeakPlayers'].sum() /
                      self.data_miner.games_df['PeakPlayers'].count()) + "\n"

        text += "Среднее количество сотрудников в компании: " + \
                str(self.data_miner.devs_df['NumberOfEmployees'].sum() / self.data_miner.devs_df[
                    'NumberOfEmployees'].count()) \
                + "\n"

        f = open(out_path, 'w')
        f.writelines(text)
        f.close()

    def handle_show_graph1(self):
        av_ratings = []
        years = sorted(list(set(self.data_miner.games_df['Year'].values)))
        for year in years:
            y_df = self.data_miner.games_df.loc[self.data_miner.games_df['Year'] == year]
            av_ratings.append(y_df['Rating'].sum() / y_df['Rating'].count())

        plt.title("Зависимость среднего рейтинга от года")
        plt.xlabel("Год")
        plt.ylabel("Ср. рейтинг")
        plt.plot(years, av_ratings)
        plt.show()

    def handle_show_graph2(self):
        ratings = []
        staff_count = []
        for isx, row in self.data_miner.games_df.iterrows():
            ratings.append(row['Rating'])
            staff_count.append(int(self.data_miner.devs_df.loc[self.data_miner.devs_df['Name']
                                                           == row['Developer']]['NumberOfEmployees']))

        plt.title("Зависимость рейтинга от количества сотрудников")
        plt.xlabel("Кол-во сотрудников")
        plt.ylabel("Рейтинг")
        plt.scatter(staff_count, ratings)
        plt.show()

    def handle_show_graph3(self):
        ratings = []
        players_count = []
        for isx, row in self.data_miner.games_df.iterrows():
            ratings.append(row['Rating'])
            players_count.append(row['PeakPlayers'])
        plt.title("Зависимость рейтинга от количества игроков")
        plt.xlabel("Кол-во игроков")
        plt.ylabel("Рейтинг")
        plt.scatter(players_count, ratings)
        plt.show()

def main():
    root = Tk()
    data_miner = DataMiner()
    MainFrame(data_miner)
    root.mainloop()


if __name__ == '__main__':
    main()
